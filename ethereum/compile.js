// Import built-in modules
const path = require('path')
const fs = require('fs-extra')

// Import downloaded modules
const solc = require('solc')

// Constants
const contractsFolder = 'contracts'

const buildPath = path.resolve(__dirname, 'build')
console.log(`Removing ${buildPath}`)
fs.removeSync(buildPath)

// console.log(fs.readdirSync(path.resolve(__dirname, contractsFolder)))

const sourceCode_Ownable = fs.readFileSync(path.resolve(__dirname, contractsFolder, 'Ownable.sol'), "utf8")
const sourceCode_Election = fs.readFileSync(path.resolve(__dirname, contractsFolder, 'Election.sol'), "utf8")
const sourceCode_ElectionFactory = fs.readFileSync(path.resolve(__dirname, contractsFolder, 'ElectionFactory.sol'), "utf8")

function compileContract(Contract) {

  console.log(`Compiling contract ${Contract[Contract.length - 1]}`)

  const contractPath = path.resolve(__dirname, ...Contract);
  const contractSourceCode = fs.readFileSync(contractPath, "utf8");

  fs.ensureDirSync(buildPath);

  var input = {
    language: "Solidity",
    sources: {
      Contract: {
        content: contractSourceCode
      }
    },
    settings: {
      optimizer: {
        enabled: true
      },
      outputSelection: {
        "*": {
          "*": ["*"]
        }
      }
    }
  };

  function findImports(path) {
    if (path === "Ownable.sol") return { contents: `${sourceCode_Ownable}` };
    if (path === "Election.sol") return { contents: `${sourceCode_Election}` };
    if (path === "ElectionFactory.sol") return { contents: `${sourceCode_ElectionFactory}` };
    else return { error: "File not found" };
  }

  let output = JSON.parse(solc.compile(JSON.stringify(input), { import: findImports }));

  for (let contractName in output.contracts.Contract) {

    const fileBuildPath = path.resolve(buildPath, `${contractName}.json`)

    console.log(`Writing file @ ${fileBuildPath}`)

    fs.outputJsonSync(
      fileBuildPath,
      output.contracts.Contract[contractName]
    );

    console.log(`Contract ${contractName} written.`)
  }

}

compileContract(["./", contractsFolder, "ElectionFactory.sol"]);
compileContract(["./", contractsFolder, "Election.sol"]);
compileContract(["./", contractsFolder, "Ownable.sol"]);

// https://ethereum.stackexchange.com/questions/103975/solc-compiler-file-import-callback-not-supported