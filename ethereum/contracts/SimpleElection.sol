pragma solidity ^0.8.7;

// SPDX-License-Identifier: GPL-3.0

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

/*
[0x5B38Da6a701c568545dCfcB03FcB875f56beddC4, 0xAb8483F64d9C6d1EcF9b849Ae677dD3315835cb2, 0x5c6B0f7Bf3E7ce046039Bd8FABdfD3f9F5021678]
["0x5B38Da6a701c568545dCfcB03FcB875f56beddC4", "0xAb8483F64d9C6d1EcF9b849Ae677dD3315835cb2", "0x5c6B0f7Bf3E7ce046039Bd8FABdfD3f9F5021678"]
*/

contract Election is Ownable, AccessControl {
    
  // Roles
  bytes32 public constant ROLE_PARTICIPANT = keccak256("ROLE_PARTICIPANT");
  bytes32 public constant ROLE_CANDIDATE = keccak256("ROLE_CANDIDATE");

  struct Candidate {

    uint256 id;
    uint voteCount;
  }

  // Election status
  bool public hasElectionStarted;
  bool public hasElectionEnded;
  bool public isElectionRunning;

  uint participantsCount;
  uint candidatesCount;
  
  mapping(address => bool) voters;
  mapping(address => Candidate) candidates;

  // Events
  event votedEvent (address indexed _candidateAddress);
  event electionStarted();
  event electionEnded();
  
  constructor(address[] memory _initialParticipants) {
    
    for(uint i=0; i<_initialParticipants.length; i++) {
        _setupRole(ROLE_PARTICIPANT, _initialParticipants[i]);
    }
  }

  function runElection (bool _status) public onlyOwner {

    if (!hasElectionStarted && _status) {
      hasElectionStarted = true;
      emit electionStarted();
    }
    
    if(!hasElectionEnded) {
      isElectionRunning = _status;
    }
  }

  function endElection () public onlyOwner {

    require(hasElectionStarted, "Election must have started to end it");
    
    if (!isElectionRunning) {
        isElectionRunning = false;
    }
    
    hasElectionEnded = true;
    
    emit electionEnded();
  }
  
  function becomeCandidate() public onlyRole(ROLE_PARTICIPANT) {
    
    require(!hasElectionStarted, "Election has already started");
    require(!hasRole(ROLE_CANDIDATE, msg.sender), "You are already candidate");
    
    candidatesCount ++;
    candidates[msg.sender] = Candidate(candidatesCount, 0);
    _setupRole(ROLE_CANDIDATE, msg.sender);
  }
  
  function vote (address _candidateAddress) public onlyRole(ROLE_PARTICIPANT) {
    
    require(isElectionRunning, "Election are not running");
    require(!voters[msg.sender], "You have already voted");
    require(hasRole(ROLE_CANDIDATE, _candidateAddress), "Require a valid candidate");
    
    // Voters has now voted
    voters[msg.sender] = true;

    // Update candidate vote Count
    candidates[_candidateAddress].voteCount ++;

    emit votedEvent (_candidateAddress);
  }
  
}