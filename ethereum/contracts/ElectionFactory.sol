pragma solidity ^0.6.12;

// SPDX-License-Identifier: GPL-3.0

import "./Election.sol";

contract ElectionFactory {

  Election[] public deployedElections;

  function createElection() public {

    Election newElection = new Election(msg.sender);
    deployedElections.push(newElection);
  }

  function getDeployedElections() public view returns (Election[] memory) {
    return deployedElections;
  }
}