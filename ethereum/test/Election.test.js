// Import build-in modules
const assert = require('assert')

// Import downloaded modules
const ganache = require('ganache-cli')
const Web3 = require('web3')

const web3 = new Web3(ganache.provider())

// Compiled contracts
const compiledFactory = require('../build/ElectionFactory.json')
const compliedContract = require('../build/Election.json')

// Constants
const NULL_ADDRESS = '0x0000000000000000000000000000000000000000'
const GAS = '5000000'
const DEFAULT_TEAM_NAME = 'Team 1'
const INT_MEMBER_STATUS_SECRETARY = 1
const INT_MEMBER_STATUS_TREASURY = 2

// Variables: contract information
let accounts
let factory
let contractAddress
let contract

// Variables: contract owner information
let contractOwner
let contractOwnerSend

// Variables: contract methods as a promise
let promiseCreateParticipant
let promiseRemoveParticipant
let promiseCreateTeam
let promiseCreateTeamMember
let promiseRunElection

// Variables: contract GET variables methods as a promise
let promiseGetParticipantsCount
let promiseGetParticipant

beforeEach(async () => {

  accounts = await web3.eth.getAccounts()
  contractOwner = accounts[0]

  contractOwnerSend = {
    from: contractOwner,
    gas: GAS
  }

  // Create the CampaignFactory contract
  factory = await new web3.eth.Contract(compiledFactory.abi)
    .deploy({ data: compiledFactory.evm.bytecode.object })
    .send(contractOwnerSend);

  // Create a Campaign contract
  await factory.methods.createElection().send(contractOwnerSend);

  // Get contract address
  [contractAddress] = await factory.methods.getDeployedElections().call()

  // Get campaign instance
  contract = await new web3.eth.Contract(
    compliedContract.abi,
    contractAddress
  )

  // Contract methods as a promise
  promiseCreateParticipant = (_address, _name) => (
    contract.methods
      .createParticipant(_address, _name)
      .send(contractOwnerSend)
  )

  promiseCreateTeam = (_address, _name) => (
    contract.methods
      .createTeam(_name)
      .send({ from: _address, gas: GAS })
  )

  promiseCreateTeamMember = (_presidentAddress, _inviteeAddress, _status) => (
    contract.methods
      .createTeamMember(_inviteeAddress, _status)
      .send({ from: _presidentAddress, gas: GAS })
  )

  promiseRunElection = (_status) => (
    contract.methods.runElection(_status).send(contractOwnerSend)
  )

  promiseRemoveParticipant = (_address) => (
    contract.methods
      .removeParticipant(_address)
      .send(contractOwnerSend)
  )

  // Contract GET methods as a promise:
  promiseGetParticipantsCount = contract.methods.participantsCount().call

  promiseGetParticipant = async (_address) => contract.methods.participants(_address).call()

})

describe('Election: Owner', () => {

  it('Deploys a factory and a election', () => {

    assert.ok(factory.options.address)
    assert.ok(contract.options.address)
  })

  it('Marks caller as the election owner', async () => {

    const owner = await contract.methods.owner().call()
    assert.equal(contractOwner, owner)
  })

  it('Can add one participant', async () => {

    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    const initialParticipantCount = await contract.methods.participantsCount().call()

    await promiseCreateParticipant(participantTest.address, participantTest.name)

    const participant = await promiseGetParticipant(participantTest.address)
    const participantCount = await contract.methods.participantsCount().call()

    assert.equal(participantCount, parseInt(initialParticipantCount) + 1)

    assert.ok(participant)
    assert.equal(participant.id, '1')
    assert.ok(!participant.hasVoted)
    assert.equal(participant.teamAddress, NULL_ADDRESS)

  })

  it('Can add multiple participants', async () => {

    // Initial participants array
    const initialParticipants = accounts.slice(1, 6)
      .map((element, index) => ({ address: element, name: `Participant ${index + 1}`, id: index + 1 }));

    const initialParticipantCount = await contract.methods.participantsCount().call();

    // Create participants
    for (let i = 0, c = initialParticipants.length; i < c; i++) {

      await promiseCreateParticipant(
        initialParticipants[i].address,
        initialParticipants[i].name
      )
    }

    // Get participants
    const participants = await Promise.all(
      initialParticipants.map(element => (
        promiseGetParticipant(element.address)
      ))
    )

    const participantCount = await contract.methods.participantsCount().call()

    // Tests
    assert.equal(participants.length, initialParticipants.length)
    assert.equal(participantCount, `${parseInt(initialParticipantCount) + participants.length}`)
    assert.equal(participantCount, `${parseInt(initialParticipantCount) + initialParticipants.length}`)

    participants.forEach((element, index) => {

      assert.ok(element)
      assert.equal(element.id, initialParticipants[index].id)
      assert.ok(!element.hasVoted)
      assert.equal(element.teamAddress, NULL_ADDRESS)
    })

  })

  it("Can't add a participant twice", async () => {

    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    await promiseCreateParticipant(participantTest.address, participantTest.name)

    try {

      await promiseCreateParticipant(participantTest.address, participantTest.name)
      assert(false)

    } catch (error) {
      assert(error)
    }

  })

  it("Can't add a participant if not owner", async () => {

    // Is invitee, but not Contract Owner
    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    // Is not Invitee, nor Contract Owner
    const participantTest2 = {
      name: 'Participant 2',
      address: accounts[2]
    }

    // Is not Invitee, nor Contract Owner
    const participantTest3 = {
      name: 'Participant 3',
      address: accounts[3]
    }

    await promiseCreateParticipant(participantTest.address, participantTest.name)

    try {

      contract.methods
        .createParticipant(participantTest3.address, participantTest3.name)
        .send({ from: participantTest.address, gas: GAS })

      assert(false)

    } catch (error) {
      assert(error)
    }

    try {

      contract.methods
        .createParticipant(participantTest3.address, participantTest3.name)
        .send({ from: participantTest2.address, gas: GAS })

      assert(false)

    } catch (error) {
      assert(error)
    }

  })

  it("Can run an election", async () => {

    let hasElectionStarted
    let isElectionRunning
    let hasElectionEnded

    hasElectionStarted = await contract.methods.hasElectionStarted().call()
    isElectionRunning = await contract.methods.isElectionRunning().call()
    hasElectionEnded = await contract.methods.hasElectionEnded().call()

    // Tests: Before election has started
    assert.ok(!hasElectionStarted)
    assert.ok(!isElectionRunning)
    assert.ok(!hasElectionEnded)

    for (let i = 0, c = 10; i < c; i++) {

      await promiseRunElection(true)

      // Retrieve data
      hasElectionStarted = await contract.methods.hasElectionStarted().call()
      isElectionRunning = await contract.methods.isElectionRunning().call()
      hasElectionEnded = await contract.methods.hasElectionEnded().call()

      // Tests: After election has started
      assert.ok(hasElectionStarted)
      assert.ok(isElectionRunning)
      assert.ok(!hasElectionEnded)

      await promiseRunElection(false)

      // Retrieve data
      hasElectionStarted = await contract.methods.hasElectionStarted().call()
      isElectionRunning = await contract.methods.isElectionRunning().call()
      hasElectionEnded = await contract.methods.hasElectionEnded().call()

      // Tests: After election has started
      assert.ok(hasElectionStarted)
      assert.ok(!isElectionRunning)
      assert.ok(!hasElectionEnded)

    }

  }).timeout(10000)

  it("Can't run election if not contract owner", async () => {

    // Is invitee, but not Contract Owner
    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    // Is not Invitee, nor Contract Owner
    const participantTest2 = {
      name: 'Participant 2',
      address: accounts[2]
    }

    await promiseCreateParticipant(participantTest2.address, participantTest2.name)

    try {

      await contract.methods.runElection(true).send({
        from: participantTest.address,
        gas: GAS
      })

      assert(false)

    } catch (error) {
      assert(error)
    }

    try {

      await contract.methods.runElection(true).send({
        from: participantTest2.address,
        gas: GAS
      })

      assert(false)

    } catch (error) {
      assert(error)
    }

    await promiseRunElection(true)

    try {

      await contract.methods.runElection(false).send({
        from: participantTest.address,
        gas: GAS
      })

      assert(false)

    } catch (error) {
      assert(error)
    }

    try {

      await contract.methods.runElection(false).send({
        from: participantTest2.address,
        gas: GAS
      })

      assert(false)

    } catch (error) {
      assert(error)
    }

  })

  it("Can end election", async () => {

    let hasElectionEnded

    // Election has not started yet
    hasElectionEnded = await contract.methods.hasElectionEnded().call()
    assert.ok(!hasElectionEnded)

    // Election starts
    await promiseRunElection(true)
    hasElectionEnded = await contract.methods.hasElectionEnded().call()
    assert.ok(!hasElectionEnded)

    // Election ends
    await contract.methods.endElection().send(contractOwnerSend)
    hasElectionEnded = await contract.methods.hasElectionEnded().call()
    assert.ok(hasElectionEnded)

  })

  it("Can't end election if not contract owner", async () => {

    // Is invitee, but not Contract Owner
    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    // Is not Invitee, nor Contract Owner
    const participantTest2 = {
      name: 'Participant 2',
      address: accounts[2]
    }

    await promiseCreateParticipant(participantTest2.address, participantTest2.name)
    await promiseRunElection(true)

    try {

      await contract.methods.endElection().send({
        from: participantTest.address,
        gas: GAS
      })

      assert(false)

    } catch (error) {
      assert(error)
    }

    try {

      await contract.methods.endElection().send({
        from: participantTest2.address,
        gas: GAS
      })

      assert(false)

    } catch (error) {
      assert(error)
    }

  })

  it("Can't add a participant when election started", async () => {

    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    await promiseRunElection(true)

    try {

      await promiseCreateParticipant(participantTest.address, participantTest.name)
      assert(false)

    } catch (error) {
      assert(error)
    }

  })

  it("Can remove participant (only non team member or non president)", async () => {

    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    await promiseCreateParticipant(participantTest.address, participantTest.name)

    const participantsCountBeforeRemoval = await promiseGetParticipantsCount()

    await promiseRemoveParticipant(participantTest.address)

    const participantsCountAfterRemoval = await promiseGetParticipantsCount()
    const removedParticipant = await promiseGetParticipant(participantTest.address)

    // Tests
    assert.equal(removedParticipant.id, '0')
    assert.equal(removedParticipant.name, '')
    assert.ok(!removedParticipant.hasVoted)
    assert.equal(removedParticipant.teamAddress, NULL_ADDRESS)

    assert.equal(
      participantsCountAfterRemoval,
      parseInt(participantsCountBeforeRemoval) - 1
    )

  })

  it("Can't remove non participant", async () => {

    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    const participantsCountBeforeRemoval = await promiseGetParticipantsCount()

    try {

      await promiseRemoveParticipant(participantTest.address)
      assert(false)

    } catch (error) {
      assert(error)
    }

    const participantsCountAfterRemoval = await promiseGetParticipantsCount()

    // Tests
    assert.equal(participantsCountAfterRemoval, participantsCountBeforeRemoval)

  })

  it("Can't remove participant if election has started", async () => {

    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    await promiseCreateParticipant(participantTest.address, participantTest.name)

    const participantsCountBeforeRemoval = await promiseGetParticipantsCount()

    await promiseRunElection(true)

    try {

      await promiseRemoveParticipant(participantTest.address)
      assert(false)

    } catch (error) {
      assert(error)
    }

    const participantsCountAfterRemoval = await promiseGetParticipantsCount()

    // Tests
    assert.equal(participantsCountAfterRemoval, participantsCountBeforeRemoval)

  })

  it("Can't remove participant if not contract owner", async () => {

    // Is invitee, but not Contract Owner
    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    // Is Invitee, but not Contract Owner
    const participantTest2 = {
      name: 'Participant 2',
      address: accounts[2]
    }

    // Is not Invitee, nor Contract Owner
    const participantTest3 = {
      name: 'Participant 3',
      address: accounts[3]
    }

    await promiseCreateParticipant(participantTest.address, participantTest.name)
    await promiseCreateParticipant(participantTest2.address, participantTest2.name)

    const participantsCountBeforeRemoval = await promiseGetParticipantsCount()

    // Invitee tries to remove other invitee
    try {

      await contract.methods.removeParticipant(participantTest2.address).send({
        from: participantTest.address,
        gas: GAS
      })

      assert(false)

    } catch (error) {
      assert(error)
    }

    assert.equal(
      await promiseGetParticipantsCount(),
      participantsCountBeforeRemoval
    )

    // Invitee tries to self remove
    try {

      await contract.methods.removeParticipant(participantTest.address).send({
        from: participantTest.address,
        gas: GAS
      })

      assert(false)

    } catch (error) {
      assert(error)
    }

    assert.equal(
      await promiseGetParticipantsCount(),
      participantsCountBeforeRemoval
    )

    // External (Non Invitee) tries to remove an Invitee
    try {

      await contract.methods.removeParticipant(participantTest.address).send({
        from: participantTest3.address,
        gas: GAS
      })

      assert(false)

    } catch (error) {
      assert(error)
    }

    assert.equal(
      await promiseGetParticipantsCount(),
      participantsCountBeforeRemoval
    )

  })

})

describe("Election: Team President", async () => {

  it("Can create a Team", async () => {

    // Test participant
    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    await promiseCreateParticipant(participantTest.address, participantTest.name)

    const participantBeforeTeamCreation = await contract.methods
      .participants(participantTest.address)
      .call()

    await promiseCreateTeam(participantTest.address, DEFAULT_TEAM_NAME)

    const participantAfterTeamCreation = await contract.methods
      .participants(participantTest.address)
      .call()

    const team = await contract.methods.teams(participantTest.address).call()

    // Test: Team President
    assert.equal(participantBeforeTeamCreation.teamAddress, NULL_ADDRESS)
    assert.equal(participantAfterTeamCreation.teamAddress, participantTest.address)

    // Tests: Team
    assert.equal(team.id, '1')
    assert.equal(team.name, DEFAULT_TEAM_NAME)
    assert.ok(!team.isVotable)
    assert.equal(team.voteCount, '0')
    assert.equal(team.memberSecretary, NULL_ADDRESS)
    assert.equal(team.memberTreasury, NULL_ADDRESS)
    assert.ok(!team.hasMemberSecretaryJoined)
    assert.ok(!team.hasMemberTreasuryJoined)
    assert.equal(team.temporaryTransferAddress, NULL_ADDRESS)

  })

  it("Can't create a team if not participant", async () => {

    // Test participant
    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    try {

      await promiseCreateTeam(participantTest.address, DEFAULT_TEAM_NAME)
      assert(false)

    } catch (error) {
      assert(error)
    }

  })

  it("Can't create a team if already team president", async () => {

    // Test participant
    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    await promiseCreateParticipant(participantTest.address, participantTest.name)
    await promiseCreateTeam(participantTest.address, DEFAULT_TEAM_NAME)

    try {

      await promiseCreateTeam(participantTest.address, DEFAULT_TEAM_NAME)
      assert(false)

    } catch (error) {
      assert(error)
    }

  })

  it("Can send an invitation (Secretary)", async () => {

    let team

    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    const participantInvited = {
      name: 'Participant 2',
      address: accounts[2]
    }

    // Create participant (President)
    await promiseCreateParticipant(participantTest.address, participantTest.name)

    // Create participant (invitee)
    await promiseCreateParticipant(participantInvited.address, participantInvited.name)

    // Participant creates a Team 
    await promiseCreateTeam(participantTest.address, DEFAULT_TEAM_NAME)

    // Tests: team before invitation
    team = await contract.methods.teams(participantTest.address).call()
    assert.ok(!team.isVotable)
    assert.equal(team.memberSecretary, NULL_ADDRESS)
    assert.equal(team.memberTreasury, NULL_ADDRESS)
    assert.ok(!team.hasMemberSecretaryJoined)
    assert.ok(!team.hasMemberTreasuryJoined)

    // Team President sends invitation
    await promiseCreateTeamMember(
      participantTest.address,
      participantInvited.address,
      INT_MEMBER_STATUS_SECRETARY
    )

    // Tests: team after invitation
    team = await contract.methods.teams(participantTest.address).call()
    assert.ok(!team.isVotable)
    assert.equal(team.memberSecretary, participantInvited.address)
    assert.equal(team.memberTreasury, NULL_ADDRESS)
    assert.ok(!team.hasMemberSecretaryJoined)
    assert.ok(!team.hasMemberTreasuryJoined)

  })

  it("Can send an invitation (Treasury)", async () => {

    let team

    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    const participantInvited = {
      name: 'Participant 2',
      address: accounts[2]
    }

    // Create participant (President)
    await promiseCreateParticipant(participantTest.address, participantTest.name)

    // Create participant (invitee)
    await promiseCreateParticipant(participantInvited.address, participantInvited.name)

    // Participant creates a Team 
    await promiseCreateTeam(participantTest.address, DEFAULT_TEAM_NAME)

    // Tests: team before invitation
    team = await contract.methods.teams(participantTest.address).call()
    assert.ok(!team.isVotable)
    assert.equal(team.memberSecretary, NULL_ADDRESS)
    assert.equal(team.memberTreasury, NULL_ADDRESS)
    assert.ok(!team.hasMemberSecretaryJoined)
    assert.ok(!team.hasMemberTreasuryJoined)

    // Team President sends invitation
    await promiseCreateTeamMember(
      participantTest.address,
      participantInvited.address,
      INT_MEMBER_STATUS_TREASURY
    )

    // Tests: team after invitation
    team = await contract.methods.teams(participantTest.address).call()
    assert.ok(!team.isVotable)
    assert.equal(team.memberSecretary, NULL_ADDRESS)
    assert.equal(team.memberTreasury, participantInvited.address)
    assert.ok(!team.hasMemberSecretaryJoined)
    assert.ok(!team.hasMemberTreasuryJoined)

  })

})



describe('Election: Participant', () => {

  it('Can join a join a team (Secretary)', async () => {

    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    const participantInvited = {
      name: 'Participant 2',
      address: accounts[2]
    }

    // Create participant (President)
    await promiseCreateParticipant(participantTest.address, participantTest.name)

    // Create participant (invitee)
    await promiseCreateParticipant(participantInvited.address, participantInvited.name)

    // Participant creates a Team 
    await promiseCreateTeam(participantTest.address, DEFAULT_TEAM_NAME)

    // Team President sends invitation
    await promiseCreateTeamMember(
      participantTest.address,
      participantInvited.address,
      INT_MEMBER_STATUS_SECRETARY
    )

    await contract.methods
      .joinTeam(participantTest.address, INT_MEMBER_STATUS_SECRETARY)
      .send({ from: participantInvited.address, gas: GAS })

    // Retrieve test data
    const participant = await promiseGetParticipant(participantInvited.address)
    const team = await contract.methods.teams(participantTest.address).call()

    // Tests
    assert.equal(participant.teamAddress, participantTest.address)
    assert.ok(team.hasMemberSecretaryJoined)
    assert.ok(!team.hasMemberTreasuryJoined)
    assert.ok(!team.isVotable)

  })



  it('Can join a join a team (Treasury)', async () => {

    const participantTest = {
      name: 'Participant 1',
      address: accounts[1]
    }

    const participantInvited = {
      name: 'Participant 2',
      address: accounts[2]
    }

    // Create participant (President)
    await promiseCreateParticipant(participantTest.address, participantTest.name)

    // Create participant (invitee)
    await promiseCreateParticipant(participantInvited.address, participantInvited.name)

    // Participant creates a Team 
    await promiseCreateTeam(participantTest.address, DEFAULT_TEAM_NAME)

    // Team President sends invitation
    await promiseCreateTeamMember(
      participantTest.address,
      participantInvited.address,
      INT_MEMBER_STATUS_TREASURY
    )

    await contract.methods
      .joinTeam(participantTest.address, INT_MEMBER_STATUS_TREASURY)
      .send({ from: participantInvited.address, gas: GAS })

    // Retrieve test data
    const participant = await promiseGetParticipant(participantInvited.address)
    const team = await contract.methods.teams(participantTest.address).call()

    // Tests
    assert.equal(participant.teamAddress, participantTest.address)
    assert.ok(!team.hasMemberSecretaryJoined)
    assert.ok(team.hasMemberTreasuryJoined)
    assert.ok(!team.isVotable)

  })

})