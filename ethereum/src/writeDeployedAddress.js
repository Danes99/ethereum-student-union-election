// Import built-in modules
const path = require('path')
const fs = require('fs-extra')

// Constants
const FILENAME = 'DEPLOYED_ADDRESS.txt'
const DESTINATION_PATH = path.resolve(__dirname, '..', 'log', FILENAME)
const OPTIONS = 'utf-8'

const writeDeployedAddress = (_address, _contractName='Contract') => {

  try {

    fs.ensureFileSync(DESTINATION_PATH)

    const text = `\n${_contractName}: ${_address}`

    fs.appendFileSync(DESTINATION_PATH, text, OPTIONS)
    
  } catch (error) {
    console.log(error)
  }
}

module.exports = writeDeployedAddress