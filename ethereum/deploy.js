// Import downloaded modules
const HDWalletProvider = require("truffle-hdwallet-provider");
const Web3 = require("web3");

// Import custom functions
const writeDeployedAddress = require('./src/writeDeployedAddress')

// Constants
const GAS = '5000000'

// Import contract
const { abi, evm: { bytecode: { object: bytecode } } }  = require('./build/ElectionFactory.json')

// API parameters
const mnemonic = process.env.MNEMONIC
const APIEndpoint = process.env.API_ENDPOINT

// Provider
const provider = new HDWalletProvider(
  mnemonic,
  APIEndpoint
)

const web3 = new Web3(provider)

const deploy = async () => {

  const accounts = await web3.eth.getAccounts()

  console.log("Attempting to deploy from account", accounts[0])

  const result = await new web3.eth.Contract(abi)
    .deploy({ data: bytecode })
    .send({ gas: GAS, gasPrice: '5000000000', from: accounts[0] });
  
  const deployedAddress = result.options.address

  console.log("Contract deployed to", deployedAddress)
  writeDeployedAddress(deployedAddress)
}

deploy()
