// Import pre-installed modules
const path = require('path')

// Import downloaded modules
const express = require('express')

// Import routers
const routerFactory = require('./routers/factory')

// Create Express.js app
const app = express()

// App Variables
app.set('AppName', 'Server')

// Serving static files in Express
app.use(express.static(path.join(__dirname, '../public/build/front')))

// Define express config
app.use(express.urlencoded({ extended: false }))
app.use(express.json()) // To parse the incoming requests with JSON payloads

// Use routers
app.use('/api/v1/factory', routerFactory)

// Send complied React.js app (UI)
app.get(
  '/*',
  async (req, res) => res.sendFile(path.join(__dirname, '../public/build/front', 'index.html'))
)

module.exports = app