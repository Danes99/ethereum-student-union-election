// Import downloaded modules
const express = require('express')

// Create new Express.js router
const router = new express.Router()

router.get(
  '/',
  async (req, res) => {

    try {

      const address = process.env.FACTORY_ADDRESS

      if (address) {

        res.send({ address })

      } else {
        res.sendStatus(404)
      }

    } catch (error) {
      res.status(500).send(error)
      console.log(error)
    }

  }
)

router.get(
  '/address',
  async (req, res) => {

    try {

      const address = process.env.FACTORY_ADDRESS

      if (address) {

        res.send(address)

      } else {
        res.sendStatus(404)
      }

    } catch (error) {
      res.status(500).send(error)
      console.log(error)
    }

  }
)

module.exports = router