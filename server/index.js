// Server Variables
const port = process.env.PORT || 80

// Import Express.js app
const app = require('./src/app')

// Run server
app.listen(
  port,
  () => console.log(`${app.get('AppName') || 'Node.js server'} listening on port ${port}!`)
)