# Election for Student Union on Ethereum Blockchain

## UML Diagram

<img
    src='./docs/img/diagram.svg'
    alt='Diagram'
/>

## Class

- Contract
- Participant
- Team

## Function

### OnlyOwner

- transferOwnershipContract: public Ownable
- createParticipant: public Ownable
- deleteParticipant: public Ownable
- runElection: public Ownable
- endElection: public Ownable

### Only president

- updatePresident: public Participant
- createTeamMember: public Participant

#### Everyone

- vote: public Participant
- createTeam: public Participant
- joinTeam: public Participant
- acceptPresidency: public Participant

### Information

- isMember
- isTeamVotable
- hasParticipantVoted
- updateParticipant
- hasElectionStarted
- isElectionRunning

## Updates

### Libraries

- OpenZeplin: whitelist.sol
- Must update Team's member status (Team class, not Participant class) when teamMember use createTeam
- Add function "End/Terminate Elections with consequences/impacts on the smart contract"
- Finish tests
