#!/bin/bash
# Does it have execute permissions? Try a chmod +x scriptname and then ./scriptname

clear

function verify_existance_of_yarn_lock () {

  # Does package-lock.json exist ?
  # local FILE=./package-lock.json
  local FILE=./yarn.lock

  if test -f "${FILE}"; then
    echo "${FILE} exists."
    # The "yarn install --frozen-lockfile" command, helps provide faster, 
    # reliable, reproducible builds for production environments.
  else
    echo "${FILE} does not exist."
    rm -rf node_modules && yarn install --frozen-lockfile
  fi
}

function verify_existance_of_node_modules () {

  # Does node_modules exist ?
  local FOLDER=./node_modules
  
  if test -r "${FOLDER}"; then
    echo "${FOLDER} exists."
  else
    echo "${FOLDER} does not exist."
    yarn add
  fi
}

function get_abs_directory_path() {

  # $1: relative filename
  # s0: absolute filename

  echo "$(cd "$(dirname "$0")" && pwd)"
  # echo "$(cd "$(dirname "$0")" && pwd)/$(basename "$1")"
}

CONST_ABS_DIRECTORY_PATH=$(get_abs_directory_path)

function move_to_project_root_directory() {

  # Move to script directory
  cd ${CONST_ABS_DIRECTORY_PATH}

  # Move to project root directory
  cd ${VAR_DIRECTORY_ROOT_RELATIVE_TO_SCRIPT}

  # https://stackoverflow.com/questions/3915040/how-to-obtain-the-absolute-path-of-a-file-via-shell-bash-zsh-sh

}

# Variables: paths
VAR_DIRECTORY_FRONT=front/
VAR_DIRECTORY_BUILD=server/public/build/
VAR_DIRECTORY_ROOT_RELATIVE_TO_SCRIPT=../

# Variables: names
VAR_NAME_BUILD_FOLDER=front/

# Main prgramm:

move_to_project_root_directory
cd ${VAR_DIRECTORY_FRONT}

# Verify necessery files and folders for build the Web App
verify_existance_of_yarn_lock
verify_existance_of_node_modules

# Build compiled React.js Application
yarn build

# Move compiled React.js application (build)
# to desired destination folder
move_to_project_root_directory
rm -rf ${VAR_DIRECTORY_BUILD}${VAR_NAME_BUILD_FOLDER}
mv ${VAR_DIRECTORY_FRONT}/build ${VAR_DIRECTORY_BUILD}${VAR_NAME_BUILD_FOLDER}

# https://reactjs.org/docs/optimizing-performance.html